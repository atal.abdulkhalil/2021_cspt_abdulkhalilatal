﻿namespace GUI
{
    partial class RestaurantF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNaam = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxStijl = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxStraat = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxHuisnr = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxPost = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxGemeente = new System.Windows.Forms.TextBox();
            this.numericUpDownAantalSter = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonRestOpslaan = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAantalSter)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "Naam";
            // 
            // textBoxNaam
            // 
            this.textBoxNaam.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxNaam.Location = new System.Drawing.Point(425, 80);
            this.textBoxNaam.Name = "textBoxNaam";
            this.textBoxNaam.Size = new System.Drawing.Size(462, 56);
            this.textBoxNaam.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 41);
            this.label2.TabIndex = 0;
            this.label2.Text = "Stijl";
            // 
            // textBoxStijl
            // 
            this.textBoxStijl.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxStijl.Location = new System.Drawing.Point(425, 188);
            this.textBoxStijl.Name = "textBoxStijl";
            this.textBoxStijl.Size = new System.Drawing.Size(462, 56);
            this.textBoxStijl.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 309);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 41);
            this.label3.TabIndex = 0;
            this.label3.Text = "Straat";
            // 
            // textBoxStraat
            // 
            this.textBoxStraat.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxStraat.Location = new System.Drawing.Point(425, 299);
            this.textBoxStraat.Name = "textBoxStraat";
            this.textBoxStraat.Size = new System.Drawing.Size(462, 56);
            this.textBoxStraat.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(55, 419);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(204, 41);
            this.label4.TabIndex = 0;
            this.label4.Text = "Huisnummeer";
            // 
            // textBoxHuisnr
            // 
            this.textBoxHuisnr.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxHuisnr.Location = new System.Drawing.Point(425, 409);
            this.textBoxHuisnr.Name = "textBoxHuisnr";
            this.textBoxHuisnr.Size = new System.Drawing.Size(462, 56);
            this.textBoxHuisnr.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(55, 522);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(141, 41);
            this.label5.TabIndex = 0;
            this.label5.Text = "Postcode";
            // 
            // textBoxPost
            // 
            this.textBoxPost.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxPost.Location = new System.Drawing.Point(425, 512);
            this.textBoxPost.Name = "textBoxPost";
            this.textBoxPost.Size = new System.Drawing.Size(462, 56);
            this.textBoxPost.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(55, 633);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 41);
            this.label6.TabIndex = 0;
            this.label6.Text = "Gemeente";
            // 
            // textBoxGemeente
            // 
            this.textBoxGemeente.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxGemeente.Location = new System.Drawing.Point(425, 623);
            this.textBoxGemeente.Name = "textBoxGemeente";
            this.textBoxGemeente.Size = new System.Drawing.Size(462, 56);
            this.textBoxGemeente.TabIndex = 1;
            // 
            // numericUpDownAantalSter
            // 
            this.numericUpDownAantalSter.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.numericUpDownAantalSter.Location = new System.Drawing.Point(425, 722);
            this.numericUpDownAantalSter.Name = "numericUpDownAantalSter";
            this.numericUpDownAantalSter.Size = new System.Drawing.Size(146, 56);
            this.numericUpDownAantalSter.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(55, 737);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(201, 41);
            this.label7.TabIndex = 0;
            this.label7.Text = "Aantal sterren";
            // 
            // buttonRestOpslaan
            // 
            this.buttonRestOpslaan.Location = new System.Drawing.Point(660, 824);
            this.buttonRestOpslaan.Name = "buttonRestOpslaan";
            this.buttonRestOpslaan.Size = new System.Drawing.Size(227, 92);
            this.buttonRestOpslaan.TabIndex = 3;
            this.buttonRestOpslaan.Text = "Opslaan";
            this.buttonRestOpslaan.UseVisualStyleBackColor = true;
            this.buttonRestOpslaan.Click += new System.EventHandler(this.buttonRestOpslaan_Click);
            // 
            // RestaurantF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(17F, 41F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 1009);
            this.Controls.Add(this.buttonRestOpslaan);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numericUpDownAantalSter);
            this.Controls.Add(this.textBoxGemeente);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxPost);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxHuisnr);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxStraat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxStijl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxNaam);
            this.Controls.Add(this.label1);
            this.Name = "RestaurantF";
            this.Text = "Restaurant toevoegen";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAantalSter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNaam;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxStijl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxStraat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxHuisnr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxPost;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxGemeente;
        private System.Windows.Forms.NumericUpDown numericUpDownAantalSter;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonRestOpslaan;
    }
}