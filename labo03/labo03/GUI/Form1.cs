﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Globals;

namespace GUI
{
    public partial class Form1 : Form
    {
        private readonly ILogicaVerwerking logicaVw;
        public Form1(ILogicaVerwerking logicaVw)
        {
            this.logicaVw = logicaVw;
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RestaurantF restaurant = new RestaurantF(logicaVw);
            restaurant.ShowDialog();
        }

        private void buttonBzoekerTv_Click(object sender, EventArgs e)
        {
            Bezoeker bezoeker = new Bezoeker(logicaVw);
            bezoeker.ShowDialog();
        }

        private void buttonMederwerkerTv_Click(object sender, EventArgs e)
        {
            MedewerkerF medewerker = new MedewerkerF(logicaVw);
            medewerker.ShowDialog();
        }

        private void buttonAanwezigheidTv_Click(object sender, EventArgs e)
        {
            AanwezigheidF aanwezigheid = new AanwezigheidF(logicaVw);
            aanwezigheid.ShowDialog();
        }

        private void buttonAanwezigheidsOpv_Click(object sender, EventArgs e)
        {
            AanwezighedenOpvragenF aanwezighedenOpvragenF = new AanwezighedenOpvragenF(logicaVw);
            aanwezighedenOpvragenF.ShowDialog();
        }
    }
}
