﻿namespace GUI
{
    partial class Bezoeker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxVnaam = new System.Windows.Forms.TextBox();
            this.textBoxFnaam = new System.Windows.Forms.TextBox();
            this.textBoxStraat = new System.Windows.Forms.TextBox();
            this.textBoxHuisnr = new System.Windows.Forms.TextBox();
            this.TextBoxPost = new System.Windows.Forms.TextBox();
            this.textBoxGemeente = new System.Windows.Forms.TextBox();
            this.textBoxLand = new System.Windows.Forms.TextBox();
            this.textBoxTelefnr = new System.Windows.Forms.TextBox();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.textBoxPostcode = new System.Windows.Forms.TextBox();
            this.buttonBezOpslaan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "Voornaam";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 207);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(183, 41);
            this.label2.TabIndex = 0;
            this.label2.Text = "Familienaam";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 336);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 41);
            this.label3.TabIndex = 0;
            this.label3.Text = "Straat";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 461);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(188, 41);
            this.label4.TabIndex = 0;
            this.label4.Text = "Huisnummer";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(48, 584);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(141, 41);
            this.label5.TabIndex = 0;
            this.label5.Text = "Postcode";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(48, 700);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 41);
            this.label6.TabIndex = 0;
            this.label6.Text = "Gemeente";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(48, 819);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 41);
            this.label7.TabIndex = 0;
            this.label7.Text = "Land";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(48, 935);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(244, 41);
            this.label8.TabIndex = 0;
            this.label8.Text = "Telefoonnummer";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(48, 1048);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(160, 41);
            this.label9.TabIndex = 0;
            this.label9.Text = "Emailadres";
            // 
            // textBoxVnaam
            // 
            this.textBoxVnaam.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxVnaam.Location = new System.Drawing.Point(418, 71);
            this.textBoxVnaam.Name = "textBoxVnaam";
            this.textBoxVnaam.Size = new System.Drawing.Size(462, 56);
            this.textBoxVnaam.TabIndex = 1;
            // 
            // textBoxFnaam
            // 
            this.textBoxFnaam.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxFnaam.Location = new System.Drawing.Point(418, 197);
            this.textBoxFnaam.Name = "textBoxFnaam";
            this.textBoxFnaam.Size = new System.Drawing.Size(462, 56);
            this.textBoxFnaam.TabIndex = 2;
            // 
            // textBoxStraat
            // 
            this.textBoxStraat.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxStraat.Location = new System.Drawing.Point(418, 326);
            this.textBoxStraat.Name = "textBoxStraat";
            this.textBoxStraat.Size = new System.Drawing.Size(462, 56);
            this.textBoxStraat.TabIndex = 3;
            // 
            // textBoxHuisnr
            // 
            this.textBoxHuisnr.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxHuisnr.Location = new System.Drawing.Point(418, 451);
            this.textBoxHuisnr.Name = "textBoxHuisnr";
            this.textBoxHuisnr.Size = new System.Drawing.Size(462, 56);
            this.textBoxHuisnr.TabIndex = 4;
            // 
            // TextBoxPost
            // 
            this.TextBoxPost.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.TextBoxPost.Location = new System.Drawing.Point(418, 574);
            this.TextBoxPost.Name = "TextBoxPost";
            this.TextBoxPost.Size = new System.Drawing.Size(462, 56);
            this.TextBoxPost.TabIndex = 5;
            // 
            // textBoxGemeente
            // 
            this.textBoxGemeente.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxGemeente.Location = new System.Drawing.Point(418, 690);
            this.textBoxGemeente.Name = "textBoxGemeente";
            this.textBoxGemeente.Size = new System.Drawing.Size(462, 56);
            this.textBoxGemeente.TabIndex = 6;
            // 
            // textBoxLand
            // 
            this.textBoxLand.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxLand.Location = new System.Drawing.Point(418, 809);
            this.textBoxLand.Name = "textBoxLand";
            this.textBoxLand.Size = new System.Drawing.Size(462, 56);
            this.textBoxLand.TabIndex = 7;
            // 
            // textBoxTelefnr
            // 
            this.textBoxTelefnr.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxTelefnr.Location = new System.Drawing.Point(418, 925);
            this.textBoxTelefnr.Name = "textBoxTelefnr";
            this.textBoxTelefnr.Size = new System.Drawing.Size(462, 56);
            this.textBoxTelefnr.TabIndex = 8;
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxEmail.Location = new System.Drawing.Point(418, 1038);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(462, 56);
            this.textBoxEmail.TabIndex = 9;
            // 
            // textBoxPostcode
            // 
            this.textBoxPostcode.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxPostcode.Location = new System.Drawing.Point(418, 574);
            this.textBoxPostcode.Name = "textBoxPostcode";
            this.textBoxPostcode.Size = new System.Drawing.Size(462, 56);
            this.textBoxPostcode.TabIndex = 5;
            // 
            // buttonBezOpslaan
            // 
            this.buttonBezOpslaan.Location = new System.Drawing.Point(649, 1166);
            this.buttonBezOpslaan.Name = "buttonBezOpslaan";
            this.buttonBezOpslaan.Size = new System.Drawing.Size(231, 88);
            this.buttonBezOpslaan.TabIndex = 10;
            this.buttonBezOpslaan.Text = "Opslaan";
            this.buttonBezOpslaan.UseVisualStyleBackColor = true;
            this.buttonBezOpslaan.Click += new System.EventHandler(this.buttonBezOpslaan_Click);
            // 
            // Bezoeker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(17F, 41F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 1325);
            this.Controls.Add(this.buttonBezOpslaan);
            this.Controls.Add(this.textBoxEmail);
            this.Controls.Add(this.textBoxTelefnr);
            this.Controls.Add(this.textBoxLand);
            this.Controls.Add(this.textBoxGemeente);
            this.Controls.Add(this.TextBoxPost);
            this.Controls.Add(this.textBoxHuisnr);
            this.Controls.Add(this.textBoxStraat);
            this.Controls.Add(this.textBoxFnaam);
            this.Controls.Add(this.textBoxVnaam);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Bezoeker";
            this.Text = "Bezoeker toevoegen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxVnaam;
        private System.Windows.Forms.TextBox textBoxFnaam;
        private System.Windows.Forms.TextBox textBoxStraat;
        private System.Windows.Forms.TextBox textBoxHuisnr;
        private System.Windows.Forms.TextBox TextBoxPost;
        private System.Windows.Forms.TextBox textBoxGemeente;
        private System.Windows.Forms.TextBox textBoxLand;
        private System.Windows.Forms.TextBox textBoxTelefnr;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.TextBox textBoxPostcode;
        private System.Windows.Forms.Button buttonBezOpslaan;
    }
}