﻿namespace GUI
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBzoekerTv = new System.Windows.Forms.Button();
            this.buttonMederwerkerTv = new System.Windows.Forms.Button();
            this.buttonRestaurantTv = new System.Windows.Forms.Button();
            this.buttonAanwezigheidTv = new System.Windows.Forms.Button();
            this.buttonAanwezigheidsOpv = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonBzoekerTv
            // 
            this.buttonBzoekerTv.Location = new System.Drawing.Point(277, 97);
            this.buttonBzoekerTv.Name = "buttonBzoekerTv";
            this.buttonBzoekerTv.Size = new System.Drawing.Size(460, 99);
            this.buttonBzoekerTv.TabIndex = 0;
            this.buttonBzoekerTv.Text = "Bezoeker toevoegen";
            this.buttonBzoekerTv.UseVisualStyleBackColor = true;
            this.buttonBzoekerTv.Click += new System.EventHandler(this.buttonBzoekerTv_Click);
            // 
            // buttonMederwerkerTv
            // 
            this.buttonMederwerkerTv.Location = new System.Drawing.Point(277, 252);
            this.buttonMederwerkerTv.Name = "buttonMederwerkerTv";
            this.buttonMederwerkerTv.Size = new System.Drawing.Size(460, 99);
            this.buttonMederwerkerTv.TabIndex = 1;
            this.buttonMederwerkerTv.Text = "Medewerker toevoegen";
            this.buttonMederwerkerTv.UseVisualStyleBackColor = true;
            this.buttonMederwerkerTv.Click += new System.EventHandler(this.buttonMederwerkerTv_Click);
            // 
            // buttonRestaurantTv
            // 
            this.buttonRestaurantTv.Location = new System.Drawing.Point(277, 405);
            this.buttonRestaurantTv.Name = "buttonRestaurantTv";
            this.buttonRestaurantTv.Size = new System.Drawing.Size(460, 99);
            this.buttonRestaurantTv.TabIndex = 2;
            this.buttonRestaurantTv.Text = "Restaurant toevoegen";
            this.buttonRestaurantTv.UseVisualStyleBackColor = true;
            this.buttonRestaurantTv.Click += new System.EventHandler(this.button3_Click);
            // 
            // buttonAanwezigheidTv
            // 
            this.buttonAanwezigheidTv.Location = new System.Drawing.Point(277, 565);
            this.buttonAanwezigheidTv.Name = "buttonAanwezigheidTv";
            this.buttonAanwezigheidTv.Size = new System.Drawing.Size(460, 99);
            this.buttonAanwezigheidTv.TabIndex = 3;
            this.buttonAanwezigheidTv.Text = "Aanwezigheid toevoegen";
            this.buttonAanwezigheidTv.UseVisualStyleBackColor = true;
            this.buttonAanwezigheidTv.Click += new System.EventHandler(this.buttonAanwezigheidTv_Click);
            // 
            // buttonAanwezigheidsOpv
            // 
            this.buttonAanwezigheidsOpv.Location = new System.Drawing.Point(277, 727);
            this.buttonAanwezigheidsOpv.Name = "buttonAanwezigheidsOpv";
            this.buttonAanwezigheidsOpv.Size = new System.Drawing.Size(460, 99);
            this.buttonAanwezigheidsOpv.TabIndex = 4;
            this.buttonAanwezigheidsOpv.Text = "Aanwezigheid opvragen";
            this.buttonAanwezigheidsOpv.UseVisualStyleBackColor = true;
            this.buttonAanwezigheidsOpv.Click += new System.EventHandler(this.buttonAanwezigheidsOpv_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(17F, 41F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1010, 1020);
            this.Controls.Add(this.buttonAanwezigheidsOpv);
            this.Controls.Add(this.buttonAanwezigheidTv);
            this.Controls.Add(this.buttonRestaurantTv);
            this.Controls.Add(this.buttonMederwerkerTv);
            this.Controls.Add(this.buttonBzoekerTv);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "Form1";
            this.Text = "Overzicht";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonBzoekerTv;
        private System.Windows.Forms.Button buttonMederwerkerTv;
        private System.Windows.Forms.Button buttonRestaurantTv;
        private System.Windows.Forms.Button buttonAanwezigheidTv;
        private System.Windows.Forms.Button buttonAanwezigheidsOpv;
    }
}

