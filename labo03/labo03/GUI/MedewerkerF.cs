﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Globals;
using Logica;

namespace GUI
{
    public partial class MedewerkerF : Form
    {
        private readonly ILogicaVerwerking logica;
        public MedewerkerF(ILogicaVerwerking logica)
        {
            InitializeComponent();
            this.logica = logica;
            comboBoxRestaurant.DataSource = logica.GeefRestaurants();
        }

        private void buttonMedewOpslaan_Click(object sender, EventArgs e)
        {
            Medewerker medewerker = new Medewerker() { FamilieNaam = textBoxFnaam.Text, Voornaam = textBoxVnaam.Text, Straat = textBoxStraat.Text, Huisnummer = Int16.Parse(textBoxHuisnr.Text), Postcode = Int16.Parse(textBoxPost.Text), Gemeente = textBoxGemeente.Text, Land = textBoxLand.Text, Telefoonnumeer = textBoxTelefNr.Text, Rol = textBoxRol.Text, Restaurant = (Restaurant)comboBoxRestaurant.SelectedItem };
            logica.VoegMedewerkterToe(medewerker);
            this.Close();
        }
    }
}
