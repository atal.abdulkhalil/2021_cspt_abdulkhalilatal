﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Globals;
using Logica;

namespace GUI
{
    public partial class Bezoeker : Form
    {
        private readonly ILogicaVerwerking logica;
        public Bezoeker(ILogicaVerwerking logica)
        {
            InitializeComponent();
            this.logica = logica;
        }

        private void buttonBezOpslaan_Click(object sender, EventArgs e)
        {
            Globals.Bezoeker bezoeker = new Globals.Bezoeker() { FamilieNaam = textBoxFnaam.Text, Voornaam = textBoxVnaam.Text, Straat = textBoxStraat.Text, Huisnummer = Int16.Parse(textBoxHuisnr.Text), Postcode = Int16.Parse(TextBoxPost.Text), Gemeente = textBoxGemeente.Text, Land = textBoxLand.Text, Telefoonnumeer = textBoxTelefnr.Text, EmailAdres = textBoxEmail.Text };
            logica.VoegBezoekerToe(bezoeker);
            this.Close();
        }
    }
}
