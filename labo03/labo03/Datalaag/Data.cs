﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Globals;
using System.Linq;

namespace Datalaag
{
    public class Data : IData
    {
        private protected IMongoCollection<Persoon> personen;
        private protected IMongoCollection<Restaurant> restauranten;
        private protected IMongoCollection<Aanwezigheid> aanwezigheiden;
        public Data()
        {
            MongoClient client = new MongoClient("mongodb+srv://root:Azerty123@cluster0.zlai0.mongodb.net/coronaTracer?retryWrites=true&w=majority");
            IMongoDatabase db = client.GetDatabase("coronaTracer");
            personen = db.GetCollection<Persoon>("Persoon");
            restauranten = db.GetCollection<Restaurant>("Restaurant");
            aanwezigheiden = db.GetCollection<Aanwezigheid>("Aanwezigheid");
        }


        public void VoegBezoekerToe(Bezoeker bezoeker)
        {
            personen.InsertOne(bezoeker);
        }
        public void VoegMedewerkterToe(Medewerker medewerker)
        {
            personen.InsertOne(medewerker);

        }
        public void VoegRestaurantToe(Restaurant restaurant)
        {
            restauranten.InsertOne(restaurant);
        }
        public void VoegAanwezigheidsToe(Aanwezigheid aanwezigheid)
        {
            aanwezigheiden.InsertOne(aanwezigheid);
        }

        public List<Restaurant> GeefRestaurants()
        {
            List<Restaurant> restauranten = new List<Restaurant>();
            restauranten = this.restauranten.AsQueryable().ToList<Restaurant>();
            return restauranten;
        }

        public List<Persoon> GeefPersonen()
        {
            List<Persoon> personen = new List<Persoon>();
            personen = this.personen.AsQueryable().ToList<Persoon>();
            return personen;
        }


        public List<Persoon> GeefAanwezigePersonen(Restaurant restaurant, DateTime dag, Shift shift)
        {
            List<Persoon> personen = new List<Persoon>();
            personen = this.aanwezigheiden.AsQueryable()
                .Where(p => p.Restaurant == restaurant && p.Dag == dag && p.Shift == shift).Select(p => p.Persoon).ToList();
            return personen;
        }
    }
}
