﻿using System;
using System.Collections.Generic;
using System.Text;
using Globals;
using Datalaag;

namespace Logica
{
    public class LogicaVerwerking : ILogicaVerwerking
    {
        private readonly IData data;
        List<Persoon> AanwezigenPersonen = new List<Persoon>();

        public LogicaVerwerking(IData data)
        {
            this.data = data;
        }

        public void VoegBezoekerToe(Bezoeker bezoeker)
        {
            data.VoegBezoekerToe(bezoeker);
        }

        public void VoegMedewerkterToe(Medewerker medewerker)
        {
            data.VoegMedewerkterToe(medewerker);
        }

        public void VoegRestaurantToe(Restaurant restaurant)
        {
            data.VoegRestaurantToe(restaurant);
        }

        public List<Restaurant> GeefRestaurants()
        {
            List<Restaurant> restaurants = new List<Restaurant>();
            restaurants = data.GeefRestaurants();
            restaurants.Sort();
            return restaurants;
        }
        public void VoegAanwezigheidsToe(Aanwezigheid aanwezigheid)
        {
            data.VoegAanwezigheidsToe(aanwezigheid);
        }
        public List<Persoon> GeefPersonen()
        {
            List<Persoon> personen = new List<Persoon>();
            personen = data.GeefPersonen();
            personen.Sort();
            return personen;
        }

        public List<Persoon> GeefAanwezigePersonen(Restaurant restaurant, DateTime dag, Shift shift)
        {
            AanwezigenPersonen = data.GeefAanwezigePersonen(restaurant, dag, shift);
            AanwezigenPersonen.Sort();
            return AanwezigenPersonen;
        }
    }
}
