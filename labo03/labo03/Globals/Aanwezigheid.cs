﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Globals
{
    public class Aanwezigheid
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string Id { get; set; }
    
        public  Restaurant Restaurant { get; set; }
     
        public  Persoon Persoon { get; set; }
     
        public DateTime Dag { get; set; }
   
        public Shift Shift { get; set; }

        public Aanwezigheid()
        {
        }
    }
}
