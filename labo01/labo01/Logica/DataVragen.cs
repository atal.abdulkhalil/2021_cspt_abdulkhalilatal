﻿using System;
using System.Collections.Generic;
using System.Text;
using Globals;
using Datalaag;

namespace Logica
{
    public class DataVragen
    {
        ADOVerbinding data;
        //DapperVerbinding data;
        private List<Restaurant> restauranten = new List<Restaurant>();
        private List<Persoon> personen = new List<Persoon>();

        public DataVragen(){
            data = new ADOVerbinding();
            //data = new DapperVerbinding();
        }

        public List<Restaurant> GeefRestaurantNamen()
        {
            restauranten = data.GeefRestauranten();
            restauranten.Sort();
            return restauranten;
        }

        public List<Persoon> GeefAanwezigePersonen(string restuNaam, string dag, Shift shift)
        {
            personen = data.GeefAanwezigePersonen(restuNaam, dag, shift);
            personen.Sort();
            return personen;
        }

    }
}
