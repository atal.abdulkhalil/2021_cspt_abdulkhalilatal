﻿namespace GUI
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxRestu = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButtonMiddag = new System.Windows.Forms.RadioButton();
            this.radioButtonAvond = new System.Windows.Forms.RadioButton();
            this.buttonZoek = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.voornaam = new System.Windows.Forms.ColumnHeader();
            this.familienaam = new System.Windows.Forms.ColumnHeader();
            this.telefoonnr = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(97, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "Restaurants";
            // 
            // comboBoxRestu
            // 
            this.comboBoxRestu.FormattingEnabled = true;
            this.comboBoxRestu.Location = new System.Drawing.Point(97, 117);
            this.comboBoxRestu.Name = "comboBoxRestu";
            this.comboBoxRestu.Size = new System.Drawing.Size(1065, 49);
            this.comboBoxRestu.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(97, 205);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 41);
            this.label2.TabIndex = 2;
            this.label2.Text = "Dag";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(97, 264);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(1065, 47);
            this.dateTimePicker1.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(97, 364);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 41);
            this.label3.TabIndex = 4;
            this.label3.Text = "Shift";
            // 
            // radioButtonMiddag
            // 
            this.radioButtonMiddag.AutoSize = true;
            this.radioButtonMiddag.Location = new System.Drawing.Point(97, 417);
            this.radioButtonMiddag.Name = "radioButtonMiddag";
            this.radioButtonMiddag.Size = new System.Drawing.Size(158, 45);
            this.radioButtonMiddag.TabIndex = 5;
            this.radioButtonMiddag.TabStop = true;
            this.radioButtonMiddag.Text = "Middag";
            this.radioButtonMiddag.UseVisualStyleBackColor = true;
            // 
            // radioButtonAvond
            // 
            this.radioButtonAvond.AutoSize = true;
            this.radioButtonAvond.Location = new System.Drawing.Point(97, 493);
            this.radioButtonAvond.Name = "radioButtonAvond";
            this.radioButtonAvond.Size = new System.Drawing.Size(140, 45);
            this.radioButtonAvond.TabIndex = 6;
            this.radioButtonAvond.TabStop = true;
            this.radioButtonAvond.Text = "Avond";
            this.radioButtonAvond.UseVisualStyleBackColor = true;
            // 
            // buttonZoek
            // 
            this.buttonZoek.Location = new System.Drawing.Point(970, 446);
            this.buttonZoek.Name = "buttonZoek";
            this.buttonZoek.Size = new System.Drawing.Size(192, 92);
            this.buttonZoek.TabIndex = 7;
            this.buttonZoek.Text = "Zoek";
            this.buttonZoek.UseVisualStyleBackColor = true;
            this.buttonZoek.Click += new System.EventHandler(this.buttonZoek_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.voornaam,
            this.familienaam,
            this.telefoonnr});
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(97, 634);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1065, 664);
            this.listView1.TabIndex = 8;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // voornaam
            // 
            this.voornaam.Text = "Voornaam";
            this.voornaam.Width = 350;
            // 
            // familienaam
            // 
            this.familienaam.Text = "Familienaam";
            this.familienaam.Width = 350;
            // 
            // telefoonnr
            // 
            this.telefoonnr.Text = "Telefoonnr";
            this.telefoonnr.Width = 350;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(17F, 41F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1289, 1397);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.buttonZoek);
            this.Controls.Add(this.radioButtonAvond);
            this.Controls.Add(this.radioButtonMiddag);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxRestu);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Aanwezigheden";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxRestu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButtonMiddag;
        private System.Windows.Forms.RadioButton radioButtonAvond;
        private System.Windows.Forms.Button buttonZoek;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader voornaam;
        private System.Windows.Forms.ColumnHeader familienaam;
        private System.Windows.Forms.ColumnHeader telefoonnr;
    }
}

