﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Globals;
using Logica;

namespace GUI
{
    public partial class Form1 : Form
    {
        DataVragen data;
        private List<Persoon> personen;
        public Form1()
        {
            InitializeComponent();
            data = new DataVragen();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (var item in data.GeefRestaurantNamen())
            {
                comboBoxRestu.Items.Add(item.Naam);
            }
            radioButtonMiddag.Checked = true;
        }

        private void buttonZoek_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            DateTime date = dateTimePicker1.Value;
            string dag = date.ToString("yyyy-MM-dd");
            string restuNaam = comboBoxRestu.GetItemText(comboBoxRestu.SelectedItem);
            Shift shift = Shift.Middag;

            if (radioButtonMiddag.Checked)
            {
                shift = Shift.Middag;
            }
            else if (radioButtonAvond.Checked)
            {
                shift = Shift.Avond;
            }

            personen = new List<Persoon>(); 
            personen = data.GeefAanwezigePersonen(restuNaam, dag, shift);

            foreach (var persoon in personen)
            {
                string[] aanwezigePersonen = { persoon.Voornaam, persoon.FamilieNaam, persoon.Telefoonnumeer };
                var listview = new ListViewItem(aanwezigePersonen);
                this.listView1.Items.Add(listview);
            }
        }
    }
}
