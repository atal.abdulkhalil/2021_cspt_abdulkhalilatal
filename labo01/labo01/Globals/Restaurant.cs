﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Globals
{
    public class Restaurant : IComparable<Restaurant>
    {
        private List<Medewerker> medewerkers = new List<Medewerker>();
        public int Id { get; set; }
        public string Naam { get; set; }
        public string Stijl { get; set; }
        public string Straat { get; set; }
        public int Huisnummer { get; set; }
        public int Postcode { get; set; }
        public string Gemeente { get; set; }
        public int AantalSterren { get; set; }

        public Restaurant()
        {
        }

        public Restaurant(int id, string naam, string stijl, string straat, int huisnummer, int postcode, string gemeente, int aantalSterren)
        {
            Id = id;
            Naam = naam ?? throw new ArgumentNullException(nameof(naam));
            Stijl = stijl ?? throw new ArgumentNullException(nameof(stijl));
            Straat = straat ?? throw new ArgumentNullException(nameof(straat));
            Huisnummer = huisnummer;
            Postcode = postcode;
            Gemeente = gemeente ?? throw new ArgumentNullException(nameof(gemeente));
            AantalSterren = aantalSterren;
        }

        public void VoegMedewerkerToe(Medewerker medewerker)
        {
            medewerkers.Add(medewerker);
        }

        public int CompareTo(Restaurant other)
        {
            return Naam.CompareTo(other.Naam);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
