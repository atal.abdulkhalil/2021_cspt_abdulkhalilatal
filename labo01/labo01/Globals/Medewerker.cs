﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Globals
{
    public class Medewerker : Persoon
    {
        public string Rol { get; set; }

        public Medewerker(int id, string familieNaam, string voornaam, string straat, int huisnummer, int postcode, string gemeente, string land, string telefoonnumeer, string rol) : base(id, familieNaam, voornaam, straat, huisnummer, postcode, gemeente, land, telefoonnumeer)
        {
            Rol = rol;
        }
    }
}
