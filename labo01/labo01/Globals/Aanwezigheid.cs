﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Globals
{
    public class Aanwezigheid
    {
        public Restaurant restaurant { get; set; }
        public Persoon persoon { get; set; }
        public DateTime Dag { get; set; }
        public Shift shift { get; set; }

        public Aanwezigheid(Restaurant restaurant, Persoon persoon, DateTime dag, Shift shift)
        {
            this.restaurant = restaurant ?? throw new ArgumentNullException(nameof(restaurant));
            this.persoon = persoon ?? throw new ArgumentNullException(nameof(persoon));
            Dag = dag;
            this.shift = shift;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
