﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Globals
{
    public class Persoon : IComparable<Persoon>
    {
        public int Id { get; set; }
        public string FamilieNaam { get; set; }
        public string Voornaam { get; set; }
        public string Straat { get; set; }
        public int Huisnummer { get; set; }
        public int Postcode { get; set; }
        public string Gemeente { get; set; }
        public string Land { get; set; }
        public string Telefoonnumeer { get; set; }

        public Persoon()
        {
        }

        public Persoon(int id, string familieNaam, string voornaam, string straat, int huisnummer, int postcode, string gemeente, string land, string telefoonnumeer)
        {
            Id = id;
            FamilieNaam = familieNaam ?? throw new ArgumentNullException(nameof(familieNaam));
            Voornaam = voornaam ?? throw new ArgumentNullException(nameof(voornaam));
            Straat = straat ?? throw new ArgumentNullException(nameof(straat));
            Huisnummer = huisnummer;
            Postcode = postcode;
            Gemeente = gemeente ?? throw new ArgumentNullException(nameof(gemeente));
            Land = land ?? throw new ArgumentNullException(nameof(land));
            Telefoonnumeer = telefoonnumeer ?? throw new ArgumentNullException(nameof(telefoonnumeer));
        }

        public Persoon(string familieNaam, string voornaam, string telefoonnumeer)
        {
            FamilieNaam = familieNaam;
            Voornaam = voornaam;
            Telefoonnumeer = telefoonnumeer;
        }

        public int CompareTo( Persoon other)
        {
            if (FamilieNaam.Equals(other.FamilieNaam))
            {
                return Voornaam.CompareTo(other.Voornaam);
            }
            else
            {
                return FamilieNaam.CompareTo(other.FamilieNaam);
            }
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Voornaam + " " + this.FamilieNaam + " " + this.Telefoonnumeer;
        }
    }
}
