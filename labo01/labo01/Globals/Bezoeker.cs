﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Globals
{
    public class Bezoeker : Persoon
    {
        public string EmailAdres { get; set; }


        public Bezoeker(int id, string familieNaam, string voornaam, string straat, int huisnummer, int postcode, string gemeente, string land, string telefoonnumeer, string emailAdres) : base(id, familieNaam, voornaam, straat, huisnummer, postcode, gemeente, land, telefoonnumeer)
        {
            EmailAdres = emailAdres;
        }
    }
}
        