CREATE DATABASE  IF NOT EXISTS `contacttracer` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `contacttracer`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: contacttracer
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aanwezigheid`
--

DROP TABLE IF EXISTS `aanwezigheid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aanwezigheid` (
  `dag` date NOT NULL,
  `shift` enum('Middag','Avond') NOT NULL,
  `persoon_id` int NOT NULL,
  `restaurant_id` int NOT NULL,
  PRIMARY KEY (`persoon_id`,`restaurant_id`),
  KEY `fk_aanwezigheid_restaurant1_idx` (`restaurant_id`),
  CONSTRAINT `fk_aanwezigheid_persoon1` FOREIGN KEY (`persoon_id`) REFERENCES `persoon` (`id`),
  CONSTRAINT `fk_aanwezigheid_restaurant1` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aanwezigheid`
--

LOCK TABLES `aanwezigheid` WRITE;
/*!40000 ALTER TABLE `aanwezigheid` DISABLE KEYS */;
INSERT INTO `aanwezigheid` VALUES ('2020-10-27','Middag',1,1),('2020-01-09','Avond',1,2),('2020-10-27','Avond',2,1),('2020-10-15','Avond',2,4),('2020-10-27','Middag',3,1),('2020-10-27','Avond',4,1),('2020-08-15','Middag',4,3),('2020-10-27','Middag',5,1),('2020-10-27','Middag',6,1);
/*!40000 ALTER TABLE `aanwezigheid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bezoeker`
--

DROP TABLE IF EXISTS `bezoeker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bezoeker` (
  `emailAdres` varchar(60) NOT NULL,
  `persoon_id` int NOT NULL,
  PRIMARY KEY (`persoon_id`),
  KEY `fk_bezoeker_persoon_idx` (`persoon_id`),
  CONSTRAINT `fk_bezoeker_persoon` FOREIGN KEY (`persoon_id`) REFERENCES `persoon` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bezoeker`
--

LOCK TABLES `bezoeker` WRITE;
/*!40000 ALTER TABLE `bezoeker` DISABLE KEYS */;
INSERT INTO `bezoeker` VALUES ('aima.akinlade@hotmail.com',2),('wouter.metsu@gmail.com',4);
/*!40000 ALTER TABLE `bezoeker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medewerker`
--

DROP TABLE IF EXISTS `medewerker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medewerker` (
  `rol` varchar(20) NOT NULL,
  `persoon_id` int NOT NULL,
  `restaurant_id` int NOT NULL,
  PRIMARY KEY (`persoon_id`,`restaurant_id`),
  KEY `fk_medewerker_persoon1_idx` (`persoon_id`),
  KEY `fk_medewerker_restaurant1_idx` (`restaurant_id`),
  CONSTRAINT `fk_medewerker_persoon1` FOREIGN KEY (`persoon_id`) REFERENCES `persoon` (`id`),
  CONSTRAINT `fk_medewerker_restaurant1` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medewerker`
--

LOCK TABLES `medewerker` WRITE;
/*!40000 ALTER TABLE `medewerker` DISABLE KEYS */;
INSERT INTO `medewerker` VALUES ('ober',1,2),('chef',3,1);
/*!40000 ALTER TABLE `medewerker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persoon`
--

DROP TABLE IF EXISTS `persoon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persoon` (
  `id` int NOT NULL AUTO_INCREMENT,
  `familieNaam` varchar(45) NOT NULL,
  `voornaam` varchar(45) NOT NULL,
  `straat` varchar(60) NOT NULL,
  `huisnummer` int NOT NULL,
  `postcode` int NOT NULL,
  `gemeente` varchar(20) NOT NULL,
  `land` varchar(20) NOT NULL,
  `telefoonnumeer` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persoon`
--

LOCK TABLES `persoon` WRITE;
/*!40000 ALTER TABLE `persoon` DISABLE KEYS */;
INSERT INTO `persoon` VALUES (1,'abdulkhalil','atal','lekestraat',35,9900,'eeklo','belgie','0458654582'),(2,'akinlade','aima','stationstraat',20,9000,'gent','belgie','014554578'),(3,'coppens','jasper','kaaistraat',15,9000,'gent','belgie','0458784565'),(4,'metsu','wouter','steenwergstraat',45,2040,'antwerpen','belgie','0455872020'),(5,'abdulkhalil','abasin','lekestraat',35,9900,'eeklo','belgie','0455657890'),(6,'abdulkhalil','tom','lekestraat',35,9900,'eeklo','belgie','0466322055');
/*!40000 ALTER TABLE `persoon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant`
--

DROP TABLE IF EXISTS `restaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restaurant` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naam` varchar(60) NOT NULL,
  `stijl` varchar(45) NOT NULL,
  `straat` varchar(100) NOT NULL,
  `huisnummer` int NOT NULL,
  `postcode` int NOT NULL,
  `gemeente` varchar(20) NOT NULL,
  `aantalSterren` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant`
--

LOCK TABLES `restaurant` WRITE;
/*!40000 ALTER TABLE `restaurant` DISABLE KEYS */;
INSERT INTO `restaurant` VALUES (1,'de Ramblas','bistro','stationstraat',55,9900,'eeklo',4),(2,'l\'Artiglio','Italiaans','Gentsesteenweg',58,9900,'eeklo',3),(3,'Keizeshof','Brasserie','Vrijdagmarkt',47,9000,'gent',5),(4,'Martinushoeve','Bistro','Spaansemolenstraat',44,2040,'antwerpen',4);
/*!40000 ALTER TABLE `restaurant` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-07  6:48:59
