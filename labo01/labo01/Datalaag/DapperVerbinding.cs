﻿using Dapper;
using Globals;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Datalaag
{
    public class DapperVerbinding
    {
        List<Restaurant> resturanten;
        List<Persoon> personen;
        public List<Restaurant> GeefRestauranten()
        {
            resturanten = new List<Restaurant>();
            using (var connection = new MySqlConnection("Data Source=atal.mysql.database.azure.com; Initial Catalog=contacttracer; UserID=atal; Password=_Azerty123"))
            {
                resturanten = connection.Query<Restaurant>("SELECT * FROM contacttracer.restaurant").ToList();
            }
            return resturanten;
        }


        public List<Persoon> GeefAanwezigePersonen(string restuNaam, string dag, Shift shift)
        {
            personen = new List<Persoon>();
            string query = "select persoon.voornaam, persoon.familieNaam, persoon.telefoonnumeer from persoon inner join aanwezigheid on persoon.id = aanwezigheid.persoon_id inner join restaurant on restaurant.id = aanwezigheid.restaurant_id where restaurant.naam =@restuNaam and dag =@dag and shift =@shift";
            var parameters = new { restuNaam = restuNaam, dag = dag, shift = shift.ToString() };
            using (var connection = new MySqlConnection("Data Source=localhost; Initial Catalog=contacttracer; UserID=root; Password=_Azerty123"))
            {
                personen = connection.Query<Persoon>(query, parameters).ToList();
            }
            return personen;
        }
    }

}

