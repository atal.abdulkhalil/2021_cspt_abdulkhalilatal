﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Globals;

namespace Datalaag
{
    public class ADOVerbinding
    {
        private MySqlConnection connection;
        private MySqlDataAdapter dataAdapter;
        private DataSet dataSet;
        public ADOVerbinding()
        {
            this.connection = new MySqlConnection("Data Source=atal.mysql.database.azure.com; Initial Catalog=contacttracer; UserID=atal; Password=_Azerty123");
        }


        public List<Restaurant> GeefRestauranten()
        {
            MySqlDataReader reader = null;
            List<Restaurant> resturanten = new List<Restaurant>();

            try
            {
                connection.Open();
                MySqlCommand leesRestturanten = new MySqlCommand("SELECT * FROM contacttracer.restaurant", connection);
                reader = leesRestturanten.ExecuteReader();

                while (reader.Read())
                {
                    resturanten.Add(new Restaurant((int)reader["id"], (string)reader["naam"], (string)reader["stijl"], (string)reader["straat"], (int)reader["huisnummer"], (int)reader["postcode"], (string)reader["gemeente"], (int)reader["aantalSterren"]));
                }
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return resturanten;
        }


        public List<Persoon> GeefAanwezigePersonen(string restuNaam, string dag, Shift shift)
        {
            List<Persoon> personen = new List<Persoon>();
            MySqlCommand query = new MySqlCommand("select persoon.voornaam, persoon.familieNaam, persoon.telefoonnumeer from persoon inner join aanwezigheid on persoon.id = aanwezigheid.persoon_id inner join restaurant on restaurant.id = aanwezigheid.restaurant_id where restaurant.naam =@restuNaam and dag =@dag and shift =@shift;", connection);
            query.Parameters.Add("@restuNaam", MySqlDbType.String, 60).Value = restuNaam;
            query.Parameters.Add("@shift", MySqlDbType.Enum, 15).Value = shift;
            query.Parameters.Add("@dag", MySqlDbType.Date, 20).Value = dag;
            dataAdapter = new MySqlDataAdapter() { SelectCommand = query };
            dataSet = new DataSet();
            dataAdapter.Fill(dataSet, "aanwezigheid");

            foreach (DataRow dr in dataSet.Tables[0].Rows)
            {
                personen.Add(new Persoon((string)dr["familieNaam"], (string)dr["voornaam"], (string)dr["telefoonnumeer"]));
            }

            return personen;
        }
    }
}
