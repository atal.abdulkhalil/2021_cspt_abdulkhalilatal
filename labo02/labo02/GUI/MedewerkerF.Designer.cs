﻿namespace GUI
{
    partial class MedewerkerF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxVnaam = new System.Windows.Forms.TextBox();
            this.textBoxFnaam = new System.Windows.Forms.TextBox();
            this.textBoxStraat = new System.Windows.Forms.TextBox();
            this.textBoxHuisnr = new System.Windows.Forms.TextBox();
            this.textBoxPost = new System.Windows.Forms.TextBox();
            this.textBoxGemeente = new System.Windows.Forms.TextBox();
            this.textBoxTelefNr = new System.Windows.Forms.TextBox();
            this.textBoxLand = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxRol = new System.Windows.Forms.TextBox();
            this.comboBoxRestaurant = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.buttonMedewOpslaan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 1042);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "Rol";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 813);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 41);
            this.label2.TabIndex = 0;
            this.label2.Text = "Land";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 694);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 41);
            this.label3.TabIndex = 0;
            this.label3.Text = "Gemeente";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 578);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 41);
            this.label4.TabIndex = 0;
            this.label4.Text = "Postcode";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(57, 455);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(188, 41);
            this.label5.TabIndex = 0;
            this.label5.Text = "Huisnummer";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(57, 330);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 41);
            this.label6.TabIndex = 0;
            this.label6.Text = "Straat";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(57, 201);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(183, 41);
            this.label7.TabIndex = 0;
            this.label7.Text = "Familienaam";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(57, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 41);
            this.label8.TabIndex = 0;
            this.label8.Text = "Voornaam";
            // 
            // textBoxVnaam
            // 
            this.textBoxVnaam.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxVnaam.Location = new System.Drawing.Point(427, 65);
            this.textBoxVnaam.Name = "textBoxVnaam";
            this.textBoxVnaam.Size = new System.Drawing.Size(462, 56);
            this.textBoxVnaam.TabIndex = 1;
            // 
            // textBoxFnaam
            // 
            this.textBoxFnaam.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxFnaam.Location = new System.Drawing.Point(427, 191);
            this.textBoxFnaam.Name = "textBoxFnaam";
            this.textBoxFnaam.Size = new System.Drawing.Size(462, 56);
            this.textBoxFnaam.TabIndex = 2;
            // 
            // textBoxStraat
            // 
            this.textBoxStraat.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxStraat.Location = new System.Drawing.Point(427, 320);
            this.textBoxStraat.Name = "textBoxStraat";
            this.textBoxStraat.Size = new System.Drawing.Size(462, 56);
            this.textBoxStraat.TabIndex = 3;
            // 
            // textBoxHuisnr
            // 
            this.textBoxHuisnr.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxHuisnr.Location = new System.Drawing.Point(427, 445);
            this.textBoxHuisnr.Name = "textBoxHuisnr";
            this.textBoxHuisnr.Size = new System.Drawing.Size(462, 56);
            this.textBoxHuisnr.TabIndex = 4;
            // 
            // textBoxPost
            // 
            this.textBoxPost.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxPost.Location = new System.Drawing.Point(427, 568);
            this.textBoxPost.Name = "textBoxPost";
            this.textBoxPost.Size = new System.Drawing.Size(462, 56);
            this.textBoxPost.TabIndex = 5;
            // 
            // textBoxGemeente
            // 
            this.textBoxGemeente.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxGemeente.Location = new System.Drawing.Point(427, 684);
            this.textBoxGemeente.Name = "textBoxGemeente";
            this.textBoxGemeente.Size = new System.Drawing.Size(462, 56);
            this.textBoxGemeente.TabIndex = 6;
            // 
            // textBoxTelefNr
            // 
            this.textBoxTelefNr.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxTelefNr.Location = new System.Drawing.Point(427, 919);
            this.textBoxTelefNr.Name = "textBoxTelefNr";
            this.textBoxTelefNr.Size = new System.Drawing.Size(462, 56);
            this.textBoxTelefNr.TabIndex = 8;
            // 
            // textBoxLand
            // 
            this.textBoxLand.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxLand.Location = new System.Drawing.Point(427, 803);
            this.textBoxLand.Name = "textBoxLand";
            this.textBoxLand.Size = new System.Drawing.Size(462, 56);
            this.textBoxLand.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(57, 929);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(244, 41);
            this.label9.TabIndex = 0;
            this.label9.Text = "Telefoonnummer";
            // 
            // textBoxRol
            // 
            this.textBoxRol.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxRol.Location = new System.Drawing.Point(427, 1032);
            this.textBoxRol.Name = "textBoxRol";
            this.textBoxRol.Size = new System.Drawing.Size(462, 56);
            this.textBoxRol.TabIndex = 9;
            // 
            // comboBoxRestaurant
            // 
            this.comboBoxRestaurant.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comboBoxRestaurant.FormattingEnabled = true;
            this.comboBoxRestaurant.Location = new System.Drawing.Point(427, 1148);
            this.comboBoxRestaurant.Name = "comboBoxRestaurant";
            this.comboBoxRestaurant.Size = new System.Drawing.Size(462, 58);
            this.comboBoxRestaurant.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(57, 1165);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(158, 41);
            this.label10.TabIndex = 0;
            this.label10.Text = "Restaurant";
            // 
            // buttonMedewOpslaan
            // 
            this.buttonMedewOpslaan.Location = new System.Drawing.Point(661, 1260);
            this.buttonMedewOpslaan.Name = "buttonMedewOpslaan";
            this.buttonMedewOpslaan.Size = new System.Drawing.Size(228, 91);
            this.buttonMedewOpslaan.TabIndex = 11;
            this.buttonMedewOpslaan.Text = "Opslaan";
            this.buttonMedewOpslaan.UseVisualStyleBackColor = true;
            this.buttonMedewOpslaan.Click += new System.EventHandler(this.buttonMedewOpslaan_Click);
            // 
            // MedewerkerF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(17F, 41F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 1384);
            this.Controls.Add(this.buttonMedewOpslaan);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.comboBoxRestaurant);
            this.Controls.Add(this.textBoxRol);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxLand);
            this.Controls.Add(this.textBoxTelefNr);
            this.Controls.Add(this.textBoxGemeente);
            this.Controls.Add(this.textBoxPost);
            this.Controls.Add(this.textBoxHuisnr);
            this.Controls.Add(this.textBoxStraat);
            this.Controls.Add(this.textBoxFnaam);
            this.Controls.Add(this.textBoxVnaam);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "MedewerkerF";
            this.Text = "Medewerker toevoegen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxVnaam;
        private System.Windows.Forms.TextBox textBoxFnaam;
        private System.Windows.Forms.TextBox textBoxStraat;
        private System.Windows.Forms.TextBox textBoxHuisnr;
        private System.Windows.Forms.TextBox textBoxPost;
        private System.Windows.Forms.TextBox textBoxGemeente;
        private System.Windows.Forms.TextBox textBoxTelefNr;
        private System.Windows.Forms.TextBox textBoxLand;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxRol;
        private System.Windows.Forms.ComboBox comboBoxRestaurant;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button buttonMedewOpslaan;
    }
}