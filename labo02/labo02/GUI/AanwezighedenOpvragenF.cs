﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Globals;
using Logica;

namespace GUI
{
    public partial class AanwezighedenOpvragenF : Form
    {
        private readonly ILogicaVerwerking logica;
        public AanwezighedenOpvragenF(ILogicaVerwerking logica)
        {
            InitializeComponent();
            this.logica = logica;
            comboBoxRestaurant.DataSource = logica.GeefRestaurants();
            comboBoxShift.DataSource = Enum.GetValues(typeof(Shift));
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonZoek_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            var personen = logica.GeefAanwezigePersonen((Restaurant)comboBoxRestaurant.SelectedItem, dateTimePicker1.Value.Date, (Shift)comboBoxShift.SelectedItem);

            foreach (var persoon in personen)
            {
                string[] aanwezigePersonen = { persoon.Voornaam, persoon.FamilieNaam, persoon.Telefoonnumeer };
                var listview = new ListViewItem(aanwezigePersonen);
                this.listView1.Items.Add(listview);
            }
        }
    }
}
