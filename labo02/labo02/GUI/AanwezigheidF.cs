﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Globals;
using Logica;

namespace GUI
{
    public partial class AanwezigheidF : Form
    {
        private readonly ILogicaVerwerking logica;
        public AanwezigheidF(ILogicaVerwerking logica)
        {
            InitializeComponent();
            this.logica = logica;
            comboBoxRestaurant.DataSource = logica.GeefRestaurants();
            comboBoxPersoon.DataSource = logica.GeefPersonen();
            comboBoxShift.DataSource = Enum.GetValues(typeof(Shift));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Aanwezigheid aanwezigheid = new Aanwezigheid() { Dag = dateTimePicker1.Value.Date, Shift = (Shift)comboBoxShift.SelectedItem, Restaurant = (Restaurant)comboBoxRestaurant.SelectedItem, Persoon = (Persoon)comboBoxPersoon.SelectedItem};
            logica.VoegAanwezigheidsToe(aanwezigheid);
            this.Close();
        }
    }
}
