﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Globals;
using Logica;

namespace GUI
{
    public partial class RestaurantF : Form
    {
        private readonly ILogicaVerwerking logica;
        public RestaurantF(ILogicaVerwerking logica)
        {
            this.logica = logica;
            InitializeComponent();
        }

        private void buttonRestOpslaan_Click(object sender, EventArgs e)
        {
            Restaurant restaurant = new Restaurant() { Naam = textBoxNaam.Text, Stijl = textBoxStijl.Text, Straat = textBoxStraat.Text, Huisnummer = Int16.Parse(textBoxHuisnr.Text), Postcode = Int32.Parse(textBoxPost.Text), Gemeente = textBoxGemeente.Text, AantalSterren = (int)numericUpDownAantalSter.Value };
            logica.VoegRestaurantToe(restaurant);
            this.Close();
        }
    }
}
