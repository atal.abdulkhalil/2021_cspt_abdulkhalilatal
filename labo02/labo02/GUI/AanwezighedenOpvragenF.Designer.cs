﻿namespace GUI
{
    partial class AanwezighedenOpvragenF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxRestaurant = new System.Windows.Forms.ComboBox();
            this.comboBoxShift = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.buttonZoek = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.Voornaam = new System.Windows.Forms.ColumnHeader();
            this.Familienaam = new System.Windows.Forms.ColumnHeader();
            this.Telefoonnummer = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "Restaurant";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(717, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 41);
            this.label2.TabIndex = 1;
            this.label2.Text = "Datum";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1456, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 41);
            this.label3.TabIndex = 2;
            this.label3.Text = "Shift";
            // 
            // comboBoxRestaurant
            // 
            this.comboBoxRestaurant.FormattingEnabled = true;
            this.comboBoxRestaurant.Location = new System.Drawing.Point(288, 103);
            this.comboBoxRestaurant.Name = "comboBoxRestaurant";
            this.comboBoxRestaurant.Size = new System.Drawing.Size(350, 49);
            this.comboBoxRestaurant.TabIndex = 3;
            // 
            // comboBoxShift
            // 
            this.comboBoxShift.FormattingEnabled = true;
            this.comboBoxShift.Location = new System.Drawing.Point(1596, 98);
            this.comboBoxShift.Name = "comboBoxShift";
            this.comboBoxShift.Size = new System.Drawing.Size(350, 49);
            this.comboBoxShift.TabIndex = 4;
            this.comboBoxShift.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(868, 101);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(500, 47);
            this.dateTimePicker1.TabIndex = 5;
            // 
            // buttonZoek
            // 
            this.buttonZoek.Location = new System.Drawing.Point(900, 239);
            this.buttonZoek.Name = "buttonZoek";
            this.buttonZoek.Size = new System.Drawing.Size(274, 90);
            this.buttonZoek.TabIndex = 6;
            this.buttonZoek.Text = "Zoek";
            this.buttonZoek.UseVisualStyleBackColor = true;
            this.buttonZoek.Click += new System.EventHandler(this.buttonZoek_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Voornaam,
            this.Familienaam,
            this.Telefoonnummer});
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(77, 379);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1869, 586);
            this.listView1.TabIndex = 7;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // Voornaam
            // 
            this.Voornaam.Text = "Voornaam";
            this.Voornaam.Width = 400;
            // 
            // Familienaam
            // 
            this.Familienaam.Text = "Familienaam";
            this.Familienaam.Width = 400;
            // 
            // Telefoonnummer
            // 
            this.Telefoonnummer.Text = "Telefoonnummer";
            this.Telefoonnummer.Width = 400;
            // 
            // AanwezighedenOpvragenF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(17F, 41F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2078, 1007);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.buttonZoek);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.comboBoxShift);
            this.Controls.Add(this.comboBoxRestaurant);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AanwezighedenOpvragenF";
            this.Text = "AanwezighedenOpvragenF";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxRestaurant;
        private System.Windows.Forms.ComboBox comboBoxShift;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button buttonZoek;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Voornaam;
        private System.Windows.Forms.ColumnHeader Familienaam;
        private System.Windows.Forms.ColumnHeader Telefoonnummer;
    }
}