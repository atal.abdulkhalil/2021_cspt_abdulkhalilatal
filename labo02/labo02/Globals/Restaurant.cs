﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Globals
{
    public class Restaurant : IComparable<Restaurant>
    {
        
        public virtual List<Medewerker> Medewerkers { get; set; }
        public int Id { get; set; }
        public string Naam { get; set; }
        public string Stijl { get; set; }
        public string Straat { get; set; }
        public int Huisnummer { get; set; }
        public int Postcode { get; set; }
        public string Gemeente { get; set; }
        public int AantalSterren { get; set; }


        public void VoegMedewerkerToe(Medewerker medewerker)
        {
            Medewerkers.Add(medewerker);
        }

        public int CompareTo(Restaurant other)
        {
            return Naam.CompareTo(other.Naam);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Naam;
        }
    }

}
