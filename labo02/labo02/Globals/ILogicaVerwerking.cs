﻿using System;
using System.Collections.Generic;

namespace Globals
{
    public interface ILogicaVerwerking
    {
        List<Persoon> GeefAanwezigePersonen(Restaurant restaurant, DateTime dag, Shift shift);
        List<Persoon> GeefPersonen();
        List<Restaurant> GeefRestaurants();
        void VoegAanwezigheidsToe(Aanwezigheid aanwezigheid);
        void VoegBezoekerToe(Bezoeker bezoeker);
        void VoegMedewerkterToe(Medewerker medewerker);
        void VoegRestaurantToe(Restaurant restaurant);
    }
}