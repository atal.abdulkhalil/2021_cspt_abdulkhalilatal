﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Globals
{
    public abstract class Persoon : IComparable<Persoon>
    {
        public int Id { get; set; }
        public string FamilieNaam { get; set; }
        public string Voornaam { get; set; }
        public string Straat { get; set; }
        public int Huisnummer { get; set; }
        public int Postcode { get; set; }
        public string Gemeente { get; set; }
        public string Land { get; set; }
        public string Telefoonnumeer { get; set; }

        public int CompareTo(Persoon other)
        {
            if (FamilieNaam.Equals(other.FamilieNaam))
            {
                return Voornaam.CompareTo(other.Voornaam);
            }
            else
            {
                return FamilieNaam.CompareTo(other.FamilieNaam);
            }
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Voornaam + " " + this.FamilieNaam;
        }
    }
}
