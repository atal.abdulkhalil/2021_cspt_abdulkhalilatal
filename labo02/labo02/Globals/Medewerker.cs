﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Globals
{
    public class Medewerker : Persoon
    {
        public string Rol { get; set; }
        public virtual Restaurant Restaurant { get; set; }
    }
}
