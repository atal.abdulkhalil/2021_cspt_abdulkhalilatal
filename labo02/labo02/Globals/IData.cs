﻿using System;
using System.Collections.Generic;

namespace Globals
{
    public interface IData
    {
        List<Persoon> GeefAanwezigePersonen(Restaurant restaurant, DateTime dag, Shift shift);
        List<Bezoeker> GeefBezoekers();
        List<Medewerker> GeefMedewerkers();
        List<Restaurant> GeefRestaurants();
        void VoegAanwezigheidsToe(Aanwezigheid aanwezigheid);
        void VoegBezoekerToe(Bezoeker bezoeker);
        void VoegMedewerkterToe(Medewerker medewerker);
        void VoegRestaurantToe(Restaurant restaurant);
    }
}