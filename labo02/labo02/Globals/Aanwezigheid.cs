﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Globals
{
    public class Aanwezigheid
    {
        [Key]
        public virtual Restaurant Restaurant { get; set; }
        [Key]
        public virtual Persoon Persoon { get; set; }
        [Key]
        public DateTime Dag { get; set; }
        [Key]
        public Shift Shift { get; set; }
    }
}
