﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Globals;
using SharpRepository.EfCoreRepository;
using System.Linq;

namespace Datalaag
{
    public class Data : IData
    {
        private readonly DbContext dataContext;
        private EfCoreRepository<Bezoeker> bezoekerRepos;
        private EfCoreRepository<Medewerker> medewerkerRepos;
        private EfCoreRepository<Restaurant> restaurantRepos;
        private EfCoreRepository<Aanwezigheid> aanwezigheidRepos;

        public Data()
        {
            dataContext = new DataContext();
            bezoekerRepos = new EfCoreRepository<Bezoeker>(dataContext);
            medewerkerRepos = new EfCoreRepository<Medewerker>(dataContext);
            restaurantRepos = new EfCoreRepository<Restaurant>(dataContext);
            aanwezigheidRepos = new EfCoreRepository<Aanwezigheid>(dataContext);
        }

        public void VoegBezoekerToe(Bezoeker bezoeker)
        {
            bezoekerRepos.Add(bezoeker);
        }
        public void VoegMedewerkterToe(Medewerker medewerker)
        {
            medewerkerRepos.Add(medewerker);

        }
        public void VoegRestaurantToe(Restaurant restaurant)
        {
            restaurantRepos.Add(restaurant);
        }
        public void VoegAanwezigheidsToe(Aanwezigheid aanwezigheid)
        {
            aanwezigheidRepos.Add(aanwezigheid);
        }

        public List<Restaurant> GeefRestaurants()
        {
            return restaurantRepos.GetAll().OrderBy(r => r.Naam).ToList();
        }

        public List<Bezoeker> GeefBezoekers()
        {
            return bezoekerRepos.GetAll().ToList();
        }

        public List<Medewerker> GeefMedewerkers()
        {
            return medewerkerRepos.GetAll().ToList();
        }

        public List<Persoon> GeefAanwezigePersonen(Restaurant restaurant, DateTime dag, Shift shift)
        {
            var personen = aanwezigheidRepos.GetAll().Where(p => p.Restaurant == restaurant && p.Dag == dag && p.Shift == shift).Select(p => p.Persoon).ToList();
            return personen;
        }
    }
}
