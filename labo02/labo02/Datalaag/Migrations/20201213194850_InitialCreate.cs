﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Datalaag.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Restaurants",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Naam = table.Column<string>(nullable: true),
                    Stijl = table.Column<string>(nullable: true),
                    Straat = table.Column<string>(nullable: true),
                    Huisnummer = table.Column<int>(nullable: false),
                    Postcode = table.Column<int>(nullable: false),
                    Gemeente = table.Column<string>(nullable: true),
                    AantalSterren = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Restaurants", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Persoon",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FamilieNaam = table.Column<string>(nullable: true),
                    Voornaam = table.Column<string>(nullable: true),
                    Straat = table.Column<string>(nullable: true),
                    Huisnummer = table.Column<int>(nullable: false),
                    Postcode = table.Column<int>(nullable: false),
                    Gemeente = table.Column<string>(nullable: true),
                    Land = table.Column<string>(nullable: true),
                    Telefoonnumeer = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    EmailAdres = table.Column<string>(nullable: true),
                    Rol = table.Column<string>(nullable: true),
                    RestaurantId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persoon", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Persoon_Restaurants_RestaurantId",
                        column: x => x.RestaurantId,
                        principalTable: "Restaurants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Aanwezigheids",
                columns: table => new
                {
                    Dag = table.Column<DateTime>(nullable: false),
                    Shift = table.Column<int>(nullable: false),
                    RestaurantId = table.Column<int>(nullable: false),
                    PersoonId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Aanwezigheids", x => new { x.RestaurantId, x.PersoonId, x.Dag, x.Shift });
                    table.ForeignKey(
                        name: "FK_Aanwezigheids_Persoon_PersoonId",
                        column: x => x.PersoonId,
                        principalTable: "Persoon",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Aanwezigheids_Restaurants_RestaurantId",
                        column: x => x.RestaurantId,
                        principalTable: "Restaurants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Aanwezigheids_PersoonId",
                table: "Aanwezigheids",
                column: "PersoonId");

            migrationBuilder.CreateIndex(
                name: "IX_Persoon_RestaurantId",
                table: "Persoon",
                column: "RestaurantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Aanwezigheids");

            migrationBuilder.DropTable(
                name: "Persoon");

            migrationBuilder.DropTable(
                name: "Restaurants");
        }
    }
}
