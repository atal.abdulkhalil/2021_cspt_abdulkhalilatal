﻿using System;
using System.Collections.Generic;
using System.Text;
using Globals;
using Microsoft.EntityFrameworkCore;

namespace Datalaag
{
    class DataContext : DbContext
    {
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Bezoeker> Bezoekers { get; set; }
        public DbSet<Medewerker> Medewerkers { get; set; }
        public DbSet<Aanwezigheid> Aanwezigheids { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options
                .UseLazyLoadingProxies()
                .UseMySql("server=localhost;database=labo2;user=root;password=_Azerty123");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Aanwezigheid>().Property<int>("RestaurantId");
            modelBuilder.Entity<Aanwezigheid>().Property<int>("PersoonId");
            modelBuilder.Entity<Aanwezigheid>().HasKey("RestaurantId", "PersoonId", "Dag", "Shift");
        }
    }
}
